; SPDX-FileCopyrightText: 2023 Ivan Vilata-i-Balaguer <ivan@selidor.net>
;
; SPDX-License-Identifier: CC0-1.0

(tissue-configuration
 #:indexed-documents (map read-gemtext-issue
                          (gemtext-files-in-directory "issues")))
